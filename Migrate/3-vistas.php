<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
$collection = $dnadb->forms;
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?
        $entidades = array();


        $SQL = "SELECT * FROM entidades";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        while ($arr = $rs->FetchRow()) {
            $entidades[$arr[ident]] = 'container.' . strtolower(utf8_encode(str_replace(' ', '_', $arr[grupo])));
        }

        $SQL = "select * from vistas";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        echo "<h1>Importando:" . $rs->RecordCount() . " Vistas</h1>";

        while ($arr = $rs->FetchRow()) {
            echo "<h3>Importando:$arr[idvista]</h3>";
            $data = array();
            $data[idform] = (int) $arr[idvista];
            $data[idapp] = (int) $arr[idap];
            $data[title] = utf8_encode($arr[nombre]);
            $data[desc] = utf8_encode($arr[descripcion]);
            $data[type] = $arr[tipo];
            if ($data[type] == "C")
                $data[type] = 'Q';
            if ($data[type] == "")
                $data[type] = 'V';
            $data['idobj'] = $data[type] . $arr[idvista];
            $data['idu'] = (int) $arr[iduser];
            $data['visible'] = $arr[visible];
            $data['redir'] = $arr[redir];
            $data['ident'] = (int) $arr[ident];
            $data['container'] = $entidades[$arr[ident]];
            $data['visible'] = $arr[visible];

            //---convert type----------
            switch ($arr['tipo']) {
                case "V": //------option List

                case "L"://------Check
                    list($data[redir], $null) = explode("*", $arr['redir']);
                    $data[mode] == explode(",", $arr['mode']);
                    break;

                case "C"://-------Buscadores
                    $data['type'] = 'Q';
                    list($data[redir], $null) = explode("*", $arr['redir']);
                    break;

                case "E"://-----Export
                    $data['type'] = 'E';
                    list($data[redir], $null) = explode("*", $arr['redir']);
                    break;

                case "X"://-----
                    $data['type'] = 'X';
                    list($data[redir], $null) = explode("*", $arr['redir']);
                    break;

                case "T"://----template?
                    $data['type'] = 'T';
                    break;

                case "H"://---Header
                    $data['type'] = 'H';
                    break;
            }
            //-------------------------

            $data['layout'] = $arr[layout];
            $data['css'] = $arr[estilo];

            $data[filterByUser] = ($arr[filterByUser] == 1) ? true : '';
            $data[filterByGroup] = ($arr[filterByGroup] == 1) ? true : '';
            //$data[idu]        =   $arr['usuario'];
            $data[cname] = $arr[nombrecontrol];
            $null = $arr[tabladest];
            $data[cols] = (int) $data[cols];
            $data[rows] = (int) $data[rows];
            $data[width] = (int) $data[width];
            $data[height] = (int) $data[height];
            //------convert filters
            $filtros = explode('|', str_replace('\\', '', $arr[idfiltroex]));

            if (count($filtros)) {
                foreach ($filtros as $filtro) {
                    if ($filtro <> '') {
                        list($idframe, $value) = explode('*', $filtro);
                        if ($idframe)
                            $data['filters'][$idframe] = $value;
                    }
                }
                if (count($data[filters])) {
                    foreach ($data[filters] as $idframe => $filter) {
                        var_dump($idframe <> '');
                        if ($idframe <> '') {
                            if ($filter) {
                                if (strstr($filter, "<>''")) {
                                    $filter = array('\$ne' => "''");
                                } else {
                                    if (strstr($filter, ","))
                                        $filter = explode(',', $filter);
                                }
                                $data[filters][$idframe] = $filter;
                            }
                        }
                    }
                }
            }
            if (!count($data[filters]))
                $data[filters] = '';
            //----get frames
            $SQL = "select preguntasvista.*,preguntas.nombrecontrol from preguntasvista inner join preguntas on preguntas.idpreg=preguntasvista.idpreg WHERE idvista=" . $arr[idvista] . " ORDER BY orden";
            $rspreg = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);

            while ($arrpreg = $rspreg->fetchRow()) {
                $thisarr = array();
                if ($arrpreg[control] == 'H')
                    $thisarr[hidden] = true;
                if ($arrpreg[control] == 'L')
                    $thisarr[locked] = true;
                //----requerido
                $SQL = "select * from requeridos WHERE idpreg=$arrpreg[idpreg] AND idform='V$data[idform]' AND requerido LIKE 'R*%'";
                $rsreq = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                if ($rsreq->RecordCount())
                    $thisarr[required] = true;
                $data[frames][(int) $arrpreg[idpreg]] = $thisarr;
                //$data[frames][(int) $arrpreg[idpreg]]=$data[framesExtra];
                //$data[framesExtra] = array_filter($data[framesExtra]);
            }
            //------ end filters------------------------------
            //-------Orden=sort-------------------
            if ($arr[orden]) {
                list($idframe, $dir, $export) = explode('*', $arr[orden]);
                if ($idframe <> '') {
                    if ($dir == 'ASC')
                        $dir = 1;
                    if ($dir == 'DESC')
                        $dir = -1;
                    $data['sortBy'] = array((int) $idframe => $dir);
                }
                if ($export <> '')
                    $data[export] = $export;
            }
            //-------Orden=sort-------------------
            //----sanitize the array
            $data = array_filter($data);
            var_dump($data);

            echo "Removing $data[idform] <br/>$data[type]<br/>";
            $collection = $dnadb->forms;
            $collection->remove(array(idform => $data[idform]), array(safe => true));
            $result = $collection->save($data, array(safe => true));
            var_dump($result);
            //$superData[]=$data;


            echo "<hr>";
        }
        //----Formularios: reallocar formularios si colisionan con una vista
//$result=$collection->batchInsert($superData,array(safe=>true));
//var_dump($result);
        ?>

    </body>
</html>