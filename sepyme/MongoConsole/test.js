db.container.proyectos_crefis.group(
    {
        key: {},
        cond: {
            '5040':{
                $exists:true
            },
            '5372':'2009',
            '4970':{
                $gte:'70',
                $ne:'80'
            }
        },
    reduce: function(obj,prev) {
        prev.csum += parseFloat(obj['5040'].replace('.','').replace(',','.'));
    },
    initial: {
        csum: 0
    }
});