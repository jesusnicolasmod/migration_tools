<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');

$rs = $dnadb->command(array('distinct' => 'forms', 'key' => 'idapp')); //--select distinct apps from FORMS
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        function fixPerm($str){
            //---reemplazo consulta por query
            $rtnstr=str_replace('VC', 'Q',$str);
            $rtnstr= str_replace('VV', 'V', $rtnstr);
            $rtnstr= str_replace('VL', 'L', $rtnstr);
            if(strstr($rtnstr,'EV') or strstr($rtnstr,'VF') or $rtnstr[0]=='F' ){
                $rtnstr='';
            }
            
            

            return utf8_encode($rtnstr);
        }
        $SQL = "select *  from org";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        echo "<h1>Importando:" . $rs->RecordCount() . " Org</h1>";

        $collection = $dnadb->groups;
        while ($arr = $rs->FetchRow()) {
            $data = array();
            $data[idgroup] = (real) $arr[idorg];
            $data[name] = utf8_encode($arr[nombre]);
            $data[desc] = utf8_encode($arr[descripcion]);
            $data[idu] = (real) $arr[idu];
            $data[id] = (real) $arr[id];

            $data[perm] = explode('*', $arr[permisos]);
            $data[perm] = array_filter($data[perm], 'trim');
            $data[perm] = array_map('fixPerm',$data[perm]);
            $data[perm] = array_filter($data[perm], 'trim');
            
            $data[idsup] = $arr[idsup];
            $data[idg] = $arr[idg];

            //----add users to group
//            $SQL="select *  from users2groups WHERE idorg=$arr[idorg]";
//            $rsu=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
//            while($arru=$rsu->FetchRow()) $data[users][]=(int)$arru[iduser];


            $data = array_filter($data);

            var_dump($data);

            echo "Removing $data[idorg] $data[name] <br/>";

            $collection->remove(array(idgroup => (real) $arr[idorg]), array(safe => true));
            $result = $collection->save($data, array(safe => true));
            var_dump($result);
            //$superData[]=$data;

            echo "$data[id]<hr>";
        }
        ?>
        <?php
        
        function clear_perm(&$perm){
            
            return true;
        }
        $SQL = "select *  from users where idusuario=-357175579";
        $SQL = "select *  from users";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        echo "<h1>Importando:" . $rs->RecordCount() . " Usuarios</h1>";
        //$forms2->debug=true;
        $collection = $dnadb->users;
        while ($arr = $rs->FetchRow()) {
            $data = array();
            $data[idu] = (real) $arr[idusuario];
            $data[idgroup] = (int) $arr[idorg];
            $data[nick] = utf8_encode($arr[nick]);
            $data[passw] = $arr[passw];
            $data[name] = utf8_encode($arr[nombre]);
            $data[lastname] = utf8_encode($arr[apellido]);
            $data[idnumber] = $arr[DNI];
            $data[birthDate] = $arr[fechanac];
            $data[email] = utf8_encode($arr[email]);
            $data[phone] = utf8_encode($arr[Telefono]);
            $data[mobile] = $arr[Celular];
            $data[address] = utf8_encode($arr[Domicilio]);
            $data[city] = utf8_encode($arr[Ciudad]);
            $data[position] = utf8_encode($arr[cargo]);

            $data[perm] = explode('*', $arr[permisos]);
            $data[perm] = array_filter($data[perm], 'trim');
            $data[perm] = array_map('fixPerm',$data[perm]);
            $data[perm] = array_filter($data[perm], 'trim');
            
            $data[gender] = $arr[Sexo];
            $data[checkdate] = $arr[Fechaent];
            $data[lastacc] = $arr[fechault];
            $data[owner] = (real) $arr[idu];
            $data[id] = (real) $arr[id];
            //--- add groups to this user
            $data[group][] = $data[idgroup];
            $SQL = "select *  from users2groups WHERE iduser=$arr[idusuario]";
            $rsu = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
            while ($arru = $rsu->FetchRow())
                $data[group][] = (int) $arru[idorg];
            $data[group] = array_unique($data[group]);
            sort($data[group]);

            $data = array_filter($data);

//            var_dump($data);

            echo "Removing $data[idu] $data[name] $data[lastname] <br/>";


            $collection->remove(array(idu => $data[idu]), array(safe => true));
            $result = $collection->save($data, array(safe => true));
//            var_dump($result);
            //$superData[]=$data;

            echo "$data[id]<hr>";
        }
        ?>
    </body>
</html>
