<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('../Migrate/cacheopciones.php');

$debug = false;

function fn($number) {
    return number_format($number, 0);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859">
        <!-- JQuery -->
        <script type="text/javascript" src="../jscript/jquery/jquery-1.3.2.js"></script>
        <!-- JQuery Table Sorter -->
        <script src="../jscript/jquery/plugins/tablesorter/jquery.tablesorter.min.js" type="text/javascript"></script>
        <!-- script src="../jscript/jquery/plugins/tablesorter/jquery.metadata.js" type="text/javascript"></script> -->
        <link rel="stylesheet" href="../jscript/jquery/plugins/tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />

        <script language="JavaScript" type="text/JavaScript">
            $(document).ready(function(){
                //                $(".tablesorter").tablesorter({debug:false,
                //                    // pass the headers argument and assing a object
                //                    headers: {
                //                        // assign the secound column (we start counting zero)
                //                        //  0: {sorter: false }
                //                    }
                //                });
                //$('.total td').css('background-color', '#D1E5BB');
            });
        </script>
        <style type="text/css">
            .main_legend{
                text-transform: capitalize;
            }
            .total td{
                text-transform: capitalize;
                font-weight: bold;
                background-color: #D1E5BB;

            }
            table{ border: 1px red solid;}
            table td,th { border: 1px blueviolet solid}
        </style>
    </head>
    <body>
        <h1>Reporte Global </h1>    TODO write content
        <h2>Todos los programas</h2>
        <?php
        $query = array('beneficios' => array('$exists' => 'true'));
        $fields = array('beneficios', '4651', 'id');
        if ($debug)
            var_dump($query, $fields);
        $empresas = $dnadb->selectCollection('container.empresas')->find($query, $fields) or die('Error');
        $b_cant = array();
        $b_cant_prov = array();
        $b_monto = array();
        $b_monto_prov = array();
        $provincias = fillops(39);
        asort($provincias);
        while ($arr = $empresas->getNext()) {
            $provincia = $arr['4651'];
            $emp_id = (double) $arr['id'];
            if ($provincia == '') {
                if ($debug) {
                    trigger_error('La Empresa:' . $arr['id'] . ' no tiene prov');
                }
            } else {


                foreach ($arr['beneficios'] as $beneficio) {

                    foreach ($beneficio as $key => $value) {
                        $IP = $value['IP'];
                        $b_monto[$key]+=$value['monto'];
                        $b_cant[$key]['E' . $emp_id]++;
                        $bp_cant[$key][$IP]++;

                        $b_monto_prov[$provincia][$key]+=$value['monto'];
                        $b_cant_prov[$provincia][$key]['E' . $emp_id]++;
                        $bp_cant_prov[$provincia][$key][$IP]++;

                        // var_dump('$key',$key,'$value',$value['monto']);
                    }
                }
            }
        }

        foreach ($bp_cant as $prog=>$value)
            arsort ($bp_cant[$prog]);
        
        var_dump('$bp_cant', $bp_cant);
        // EMPRESAS calculo las cantidades totales
        foreach ($b_cant as $key => $value) {
            $bemp_cant[$key] = count($value);
        }
        // EMPRESAS calculo las cantidades totales x prov
        foreach ($b_cant_prov as $prov => $b_cant) {
            foreach ($b_cant as $prog => $value) {
                $bemp_cant_prov[$prov][$prog] = count($value);
            }
        }
        // PROYECTOS
        foreach ($bp_cant as $key => $value) {
            $bproy_cant[$key] = count($value);
        }

        // PROYECTOS calculo las cantidades totales x prov
        foreach ($bp_cant_prov as $prov => $b_cant) {
            foreach ($b_cant as $prog => $value) {
                $bproy_cant_prov[$prov][$prog] = count($value);
            }
        }


        $rowspan = count($b_monto) + 2;
        if ($debug) {
            var_dump('$programas', $programas);
            echo '<hr/>';
            var_dump('$b_monto', $b_monto);
            echo '<hr/>';
            var_dump('$b_monto_prov', $b_monto_prov);
            echo '<hr/>';
            var_dump('$b_cant', $bn_cant);
            echo '<hr/>';
            var_dump('$b_cant_prov', $b_cant_prov);
            echo '<hr/>';
        }
        ?>
        <table class="tablesorter1" cellpadding="2" cellspacing="0">
            <!-- HEAD -->
            <!-- MONTOS -->
            <tr>
                <th rowspan="<?= $rowspan; ?>" class="main_legend" valign="middle">MONTOS</th>
                <th>Programa</th>
                <th>Total</th>
                <?php
                foreach ($provincias as $key => $value)
                    echo "<th>$value</th>";
                ?>
            </tr>
            <!-- BODY -->

            <?php
                foreach ($b_monto as $programa => $monto) {
            ?>
                    <tr>

                        <td><?= $programa; ?></td>
                        <td><?= fn($monto); ?></td>
                <?php
                    foreach ($provincias as $key => $value)
                        echo '<td>' . fn($b_monto_prov[$key][$programa]) . ' </td>';
                ?>
                </tr>
            <?php
                }
            ?>

                <tr class="total">
                    <td>Total:</td>
                    <td><?= fn(array_sum((array) $b_monto)); ?></td>

                <?php
                foreach ($provincias as $key => $value)
                    echo '<td>' . fn(array_sum((array) $b_monto_prov[$key])) . ' </td>';
                ?>
            </tr>
            <!-- CANTIDADES -->
            <!-- CANTIDADES -->
            <!-- CANTIDADES -->
            <!-- CANTIDADES -->
            <!-- CANTIDADES -->
            <tr>
                <th rowspan="<?= $rowspan; ?>" class="main_legend" valign="middle">EMPRESAS</th>
                <th>Programa</th>
                <th>Total</th>
                <?php
                foreach ($provincias as $key => $value)
                    echo "<th>$value</th>";
                ?>
            </tr>
            <!-- BODY -->

            <?php
                foreach ((array) $bemp_cant as $programa => $cant) {
            ?>
                    <tr>
                        <td nowrap ><?= $programa; ?></td>
                        <td><?= $cant; ?></td>
                <?php
                    foreach ($provincias as $prov => $value)
                        echo '<td>' . $bemp_cant_prov[$prov][$programa] . ' </td>';
                ?>
                </tr>
            <?php
                }
            ?>

                <tr class="total">
                    <td>Total:</td>
                    <td><?= array_sum((array) $bemp_cant); ?></td>

                <?php
                foreach ($provincias as $key => $value)
                    echo '<td>' . array_sum((array) $bemp_cant_prov[$key]) . ' </td>';
                ?>
            </tr>
            <!-- PROYECTOS -->
            <!-- PROYECTOS -->
            <!-- PROYECTOS -->
            <!-- PROYECTOS -->
            <!-- PROYECTOS -->

            <tr>
                <th rowspan="<?= $rowspan; ?>" class="main_legend" valign="middle">Proyectos</th>
                <th>Programa</th>
                <th>Total</th>
                <?php
                foreach ($provincias as $key => $value)
                    echo "<th>$value</th>";
                ?>
            </tr>
            <!-- BODY -->

            <?php
                foreach ((array) $bproy_cant as $programa => $cant) {
            ?>
                    <tr>
                        <td nowrap ><?= $programa; ?></td>
                        <td><?= $cant; ?></td>
                <?php
                    foreach ($provincias as $prov => $value)
                        echo '<td>' . $bproy_cant_prov[$prov][$programa] . ' </td>';
                ?>
                </tr>
            <?php
                }
            ?>

                <tr class="total">
                    <td>Total:</td>
                    <td><?= array_sum((array) $bproy_cant); ?></td>

                <?php
                foreach ($provincias as $key => $value)
                    echo '<td>' . array_sum((array) $bproy_cant_prov[$key]) . ' </td>';
                ?>
            </tr>
        </table>
    </body>
</html>



