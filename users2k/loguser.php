<?php
session_start();
//----------Inclusion de los archivos DB desde donde sea--------------
include('basedir.php');
require($mypath."/Connections/dnadb.php");
//--------------------------------------------------------------------
if (!isset($idu)) {
    $idu=$_SESSION['idu'];
}
$ip=$_ENV["REMOTE_ADDR"];
if ($ip=="") {
    $ip=$_SERVER["REMOTE_ADDR"];
}
if (!isset($action)) {
    $action=$_SERVER['PATH_INFO'];
}
if (isset($_SERVER['HTTP_REFERER'])) {
    $referer=$_SERVER['HTTP_REFERER'];
} else {
    $referer="";
}
$arr=array(action=>$action,referer=>$referer,ip=>$ip,checkdate=>date("Y-m-d H:i:s"),iduser=>$idu);
$dnadb->userlog->save($arr,array(safe=>true));
?>